<?php
namespace app\common\service;
use think\Model;

// This snippet is to get Redis service into a class for further use
class Redisdb extends Model
{
	private $redis=null;
	function __construct()
    {
        $host = '127.0.0.1';
	    $port = 6379;

	    $this->redis = new \Redis();
	    $this->redis->connect($host, $port);

	    $this->redis->select(0);
    }

    // Check if the key existed
    public function exist($store,$key){
    	return $redis->hExists($store,$key);
   

    //get all data from memory database
    public function get_all($store){
    	return $this->redis->hGetAll($store);
    }

	//get a data from memory database depending on key
	public function get_data($store,$key){
		$res=$this->redis->hGet($store, $key);
		return json_decode($res,true);
	}

	//set key and value into memory database
	public function set_data($store,$key,$value){
		return $this->redis->hSet($store, $key,json_encode($value));
	}

	//clean all data in memory database
	public function flush(){
		return $this->redis->flushall();
	}
}