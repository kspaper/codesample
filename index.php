<?php
namespace app\cron\model;
use think\Model;

// This is a code snippet for curling a website 
// to get cities info., categories and its content list
// and then store into designed database

class index extends Model
{
    // Getting cities' names 
    public function get_cities()
    {
        $res = curl_contents();
        $ul_content = expl('<ul class="citys_text">','</ul>', $res);
        $arr = to_array('<li>', $ul_content);
        
        $cities = db('cities');
        
        $datas = [];

        foreach ($arr as $key => $value) {
            if(empty($value)) continue;
                
                $info = expl('<div class="city_line">','</div>', $value);
                $arr = to_array('<a href=', $info);
                
                foreach ($arr as $key => $value) {
                   if(empty($value)) continue;

                    $url = expl('"','"', $value);
                    $city = explode(">",$value);

                    if($url <> '' && $city[1] <> '') {
                        $datas[]=array(
                           'url' => $url,
                           'name' => str_cleaner($city[1]),
                           'create_time' => time(),
                           'flag' => 0
                        );
                    }
                }      
        }

        $cities->insertAll($datas);
        unset($datas);

        return "Cities are all ready";
    }

    // Getting category info. under each city
    public function get_section_category()
    {
        $cities = db('cities');
        $where = 'cities.add_time is null';
        $result = $cities->where($where)->find();

        $count = mb_strlen($result['name']);

        $categories = db('categories');
        
        if ($result <> null) {

            $url = $result['url'];
            $id = $result['id'];

            $res = curl_contents($url);
            $sec_content = expl('<div class="container category-content">','</div><script ', $res);
            $arr = to_array('<section style=', $sec_content);
            
            $datas = [];

            foreach ($arr as $key => $value) {

                $sub = to_array('<section class="section-box category-cheliang">', $value);

                foreach ($sub as $key => $value) {
                    if(empty($value)) continue;

                    $info = expl('<div class="title">','</div>', $value);
                    $url = expl('<a href="', '"', $info);
                    
                    $insert_title = str_cleaner($info);
                    $final_title = mb_substr($insert_title, $count, strlen($insert_title));

                    if($url <> ''){

                        $datas[] = array(
                            'title' => $final_title,
                            'url' => $url,
                            'pid' => 0,
                            'siteid' => 2,
                            'cityid' => $id,
                            'create_time' => time(),
                            'flag'  =>0
                        );
                    } 
                }
            }

            $categories->insertAll($datas);
            unset($datas);
            $cities->where('id',$id)->update([
                'add_time' => date('Y-m-d H:i:s')
            ]);
        
            return "City:" . $result['name'] . " => " ."has done !";

        } else {
            return "Noting to proceed!";
        }

    }

    // getting content list under each category
    public function get_content(){
        $categories = db('categories');
        $content = db('content');

        $where = 'categories.pid > 0 and categories.update_time is NULL';
        $result = $categories->where($where)->find();
        //var_dump($result);
        
        if($result <> null){
            $pid = $result['pid'];
            $parent = $categories->field('title')->where('id',$pid)->find();
            
            $datas = [];

            $url = $result['url'];
            $id = $result['id'];

            $res = curl_contents($url);
            $cont = expl('<ul class="info_cont">','</ul>', $res);
            $arr = to_array('<li>',$cont);

            foreach ($arr as $key => $value) {
                if(empty($value)) continue;

                $info = expl('<div class="pic">','</div>', $value);
                $url = expl('<a href="', '"', $info);
                $title = expl('title="','"',$info);

                if ($url <> '' && $title <> '') {
                    $datas[]=array(
                        'url' => $url,
                        'title' => $title,
                        'cateid' => $id,
                        'siteid' => 2,
                        'create_time' => time(),
                        'flag' => 0 
                    );
                }
            }

            if (!empty($datas)) {
                $content->insertAll($datas);
                unset($datas);
                $categories->where('id',$id)->update([
                    'update_time' => date('Y-m-d H:i:s')
                ]);

                return $result['id'] . " has done !";
            }   
        } 
        else {
            return "Noting to proceed!";
        }
    }

    